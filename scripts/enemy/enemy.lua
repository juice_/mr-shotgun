-------------------
-- enemy classes --

-- font for rendering enemy

enemy = entity:new({
	x = 800,
	y = 800,
	size = 32,
	lastX = 0,
	lastY = 0,
	health = 100,
	maxHealth = 100,
	rotation = 0,
	speed = 100,
	range = 300,
	damage = 2,
	velocity = 0,
	maxVelocity = 1.0,
	collide = true,
	name = "HostileEntity",
	entitiyType = "enemyai",
})

enemy.hitbox = {x = enemy.x, y = enemy.y, w = 48, h = 48}

function enemy:update(dt)

	if self.health <= 0 then
		self:remove()
	end

	self.health = self.health + self.regenSpeed

	self.hitbox.x = self.x - 24
	self.hitbox.y = self.y - 24

	-- record safe position
	if self.collide == true then
		self.lastX = self.x
		self.lastY = self.y
	end

	self.collide = true

	-- direction
	self.rotation = math.atan2(player.y - self.y, player.x - self.x)

	-- move enemy
	self.x = self.x + ((math.cos(self.rotation) * (self.speed * self.velocity)) * dt)
	self.y = self.y + ((math.sin(self.rotation) * (self.speed * self.velocity)) * dt)

	-- if we're near the player then move that nigga
	if dist(self.x, self.y, player.x, player.y) <= 300 then
		self.velocity = self.velocity + 2 * dt
	else
		if self.velocity > 0.3 then
			self.velocity = self.velocity - 1.4 * dt
		elseif self.velocity < -0.3 then
			self.velocity = self.velocity + 1.4 * dt
		elseif 0.3 >= math.abs(self.velocity) then
			self.velocity = 0
		end
	end

	-- collision with player
	if aabb(self.x - 24, self.y - 24, self.size + 16, self.size + 16, player.x - 16, player.y - 16, 32, 32) then
		player.velocity = -player.velocity

		player.x = player.lastX
		player.y = player.lastY

		self.velocity = -self.velocity/2
		self.x = self.lastX
		self.y = self.lastY

		player.health = player.health - self.damage

	end

		-- capping speed
	if self.velocity > self.maxVelocity then
		self.velocity = self.maxVelocity
	elseif self.velocity < (-self.maxVelocity) then
		self.velocity = (-self.maxVelocity)
	end

	-- collisioms with other shit
	for i, v in ipairs(currentMap.entities) do

		-- collision with bullet
		if v.entityType == "bullet" then
			if aabb(self.x - 24, self.y - 24, self.size + 16, self.size + 16, v.x, v.y, v.size, v.size) then
				for i = 1, 200 do
					table.insert(currentMap.entities, particle:new({x = v.x, y = v.y, particleType = "bloodsplat", lifeLimit = 0.2}))
				end
				self.health = self.health - v.damage

				v:remove()
				self.velocity = self.velocity - 0.5

			end

		-- collisions with other enemies
		elseif v.entityType == "enemyai" then
			if aabb(self.hitbox.x - cameraX, self.hitbox.y - cameraY, self.hitbox.w, self.hitbox.h) then
				self.x = self.lastX
				self.y = self.lastY
				self.velocity = -self.velocity
			end
		end
	end

	for i, v in ipairs(currentMap.tiledata) do
		for j, k in ipairs(v) do
				-- this works but I don't like it.
			if aabb(self.x - 24, self.y - 24, self.size + 16, self.size + 16, j*64, i*64, 64, 64) then
				if k == tiles.WALL1 or k == tiles.WALL2 then
					self.x = self.lastX
					self.y = self.lastY
				end
			end
		end
	end

end

function enemy:draw()

	love.graphics.setColor(200, 200, 200)
	love.graphics.rectangle("line", self.x-1 - 40 - cameraX, self.y-1 - 46 - cameraY, 80+2, 10+2)

	love.graphics.setColor(255, 0, 0)
	love.graphics.rectangle("fill", self.x - 40 - cameraX, self.y - 46 - cameraY, 80, 10)

	love.graphics.setColor(0, 255, 0)
	love.graphics.rectangle("fill", self.x - 40 - cameraX, self.y - 46 - cameraY, (80) / (self.maxHealth) * (self.health), 10)

	love.graphics.setColor(0, 0, 255)
	love.graphics.circle("fill", self.x - cameraX, self.y - cameraY, self.size, 10000)
end

--require "scripts/enemies"