particle = entity:new({
	entityType = "particle",
	particleType = "spark",
	x, y, w = 2, h = 2,
	lifeTime = 0,
	lifeLimit = 0.1,
})

function particle:spawn(xa, ya)
	return particle:new({x = xa, y = ya})
end

function particle:draw()

	if self.particleType == "spark" then
		love.graphics.setColor(215, 50, 50)
		love.graphics.rectangle("fill", self.x - cameraX, self.y - cameraY, self.w, self.h)
	elseif self.particleType == "bloodsplat" then
		love.graphics.setColor(255, 15, 15)
		love.graphics.rectangle("fill", self.x - cameraX, self.y - cameraY, self.w, self.h)
	elseif self.particleType == "bloodpool" then
		love.graphics.setColor(255, 15, 15)
		love.graphics.circle("fill", self.x - cameraX, self.y - cameraY, self.size, self.size*2)
	end
end

function particle:update(dt)
	self.lifeTime = self.lifeTime + 1 * dt

	if self.lifeTime >= self.lifeLimit then
		self:remove()
	end

	if self.particleType == "spark" or self.particleType == "bloodsplat" then
		self.x = self.x + math.random(-4, 4)
		self.y = self.y + math.random(-4, 4)
	end
end