----------------------------
-- serialised item system --
----------------------------


----------------------------
-- Base item for creation --
Item = metaClass:new({disName = "missingno", id = 0, stack = 1,
	rclick = function()

	end,

	lclick = function()

	end,
})

---------------------------------------------------------------
-- the rest of these items below


pistol = Item:new({disname = "Pistol", id = 1, accuracy = 2
	lclick = function() 

	end,

})

tec9 = Item:new({disname = "TEC 9", id = 2, accuracy = 3
	})

shotgun = Item:new({disname = "Shotgun", id = 3, accuracy = 5, usetimer = 0.5
	lclick = function()
		createBullet(player.rotation+math.random(-accuracy, accuracy), player.x, player.y)
	end
})