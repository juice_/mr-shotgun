-- bullet class

bullet = entity:new({entityType = "bullet", size, x, y, speed, damage, rotation})

function bullet:spawn(a, b, c, d, e, f)
	return bullet:new({size = a, x = b, y = c, speed = d, rotation = e, damage = f})
end

function bullet:update(dt)
	self.x = self.x + (math.cos(self.rotation) * self.speed * dt)
	self.y = self.y + (math.sin(self.rotation) * self.speed * dt)

	for i, v in ipairs(currentMap.tiledata) do
		for j, k in ipairs(v) do
			if k == tiles.WALL1 or k == tiles.WALL2 then
				if aabb(self.x-self.size, self.y-self.size, self.size*2, self.size*2, j*64, i*64, 64, 64) then
					for i = 1, 20 do

						table.insert(currentMap.entities, particle:spawn(self.x, self.y))

					end
					self:remove()
				end
			end
		end
	end
end

function bullet:draw()
	love.graphics.setColor(50, 50, 50)
	love.graphics.circle("fill", self.x - cameraX, self.y - cameraY, self.size, self.size*2)
end