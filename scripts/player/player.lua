-------------------------------
-- controllable self class --
-------------------------------

-- create the character

player = entity:new({
	health = 20,
	maxHealth = 20,
	inventory = {},
	x = 18*64,
	y = 18*64,
	lastX = 0,
	lastY = 0,
	rotation = 0,
	velocity = 0,
	speed = 200.0,
	maxVelocity = 1.0,
	collide = true,
	canshoot = false,
})

function player:update(dt)

	if self.health <= 0 then
		self.health = self.maxHealth
		INGAME = false
		self.canshoot = false
	end

	cameraX = (self.x) - (WIDTH/2)
	cameraY = (self.y) - (HEIGHT/2)

	self.lastX = self.x
	self.lastY = self.y

	-- keyboard and mouse stoof
	if love.keyboard.isDown("w", "s") then

		if love.keyboard.isDown("w") then
			self.velocity = self.velocity + 2 * dt
		elseif love.keyboard.isDown("s") then
			self.velocity = self.velocity - 1.2 * dt
		end
	else
		if self.velocity > 0.3 then
			self.velocity = self.velocity - 1.4 * dt
		elseif self.velocity < -0.3 then
			self.velocity = self.velocity + 1.4 * dt
		elseif 0.3 >= math.abs(self.velocity) then
			self.velocity = 0
		end
	end

	if love.mouse.isDown("l") and self.canshoot == true then
		local cloneSound = data.sound.shotgun:clone()
		
		self.canshoot = false
		for i = 1, 3 do
			table.insert(currentMap.entities, bullet:spawn(3, self.x, self.y, 1000, math.atan2(mouseY - HEIGHT/2 + math.random(-50, 50), mouseX - WIDTH/2 + math.random(-50, 50)), math.random(3, 7)))
		end
		
		cloneSound:play()
	end

	if not love.mouse.isDown("l") then
		self.canshoot = true
	end

	-- rotation & movement
	
	self.rotation = math.atan2(mouseY - HEIGHT/2, mouseX - WIDTH/2)
	self.x = self.x + ((math.cos(self.rotation) * (self.speed * self.velocity)) * dt)
	self.y = self.y + ((math.sin(self.rotation) * (self.speed * self.velocity)) * dt)

	-- collision

	for i, v in ipairs(currentMap.tiledata) do
		for j, k in ipairs(v) do
				-- this works but I don't like it.
			if aabb(self.x-16, self.y-16, 32, 32, j*64, i*64, 64, 64) then
				if k == tiles.WALL1 or k == tiles.WALL2 then
					self.x = self.lastX
					self.y = self.lastY
				end
			end
		end
	end


	-- capping speed
	if self.velocity > self.maxVelocity then
		self.velocity = self.maxVelocity
	elseif self.velocity < (-self.maxVelocity) then
		self.velocity = (-self.maxVelocity)
	end
end

function player:draw()
	if DEBUG then
		love.graphics.setColor(255,0,128)
		love.graphics.rectangle("fill",player.x - 16 - cameraX, self.y-16 - cameraY, 32, 32)

		love.graphics.setColor(255,0,0)
		love.graphics.line(player.x - cameraX, player.y - cameraY, mouseX, mouseY)
	end

	love.graphics.setColor(255,255,255)
	love.graphics.draw(data.textures.player, WIDTH/2, HEIGHT/2, self.rotation, 1, 1, 24, 24)
end