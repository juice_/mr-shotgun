button = metaClass:new({x = 0, y = 0, w = 7, h = 1, canClick = false, mouseover = false,clicked = function() end, text = "Button", tooltip = "", mouseover = "colorchange"})


-- droow it
function button:draw()
    -- outline
    if self.mouseover == true then
        love.graphics.setColor(204,120,41)
        love.graphics.rectangle("fill", self.x*32-3, self.y*32-3, self.w*32+6, self.h*32+6)
    else
        love.graphics.setColor(179,179,179)
        love.graphics.rectangle("fill", self.x*32, self.y*32, self.w*32, self.h*32)
    end

    love.graphics.setColor(0, 0, 0)
    love.graphics.setFont(data.fonts.pix40)
    love.graphics.print(self.text, self.x*32+4, self.y*32+5)

    if self.mouseover == true then
        if self.tooltip == "NOTOOLTIP" then

        else
            love.graphics.setColor(80,80,80)
            love.graphics.rectangle("fill", love.mouse.getX()+2, love.mouse.getY()-30, self.tooltip:len()*8, 20)

            love.graphics.setFont(data.fonts.pix24)
            love.graphics.setColor(255,255,255)
            love.graphics.print(self.tooltip, love.mouse.getX()+4, love.mouse.getY()-27)
        end
    end
end

function button:update(dt)
    if aabb(love.mouse.getX(), love.mouse.getY(), 1, 1, self.x*32, self.y*32, self.w*32, self.h*32) then
        self.mouseover = true
    else
        self.mouseover = false
    end

    if love.mouse.isDown("l") and self.canClick == true then
        if self.mouseover then
            self.clicked()
        end

        self.canClick = false
    end

    if love.mouse.isDown("l") == false then
        self.canClick = true
    end

end