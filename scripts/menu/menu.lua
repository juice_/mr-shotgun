-----------------------------
-- Very shitty menu system --
-----------------------------

-- fonts

require "scripts/menu/button"
require "scripts/menu/slider"

-- menu class
menu = metaClass:new({
    main = {
        button:new({text = "Play", x = 5, y = 8, tooltip = "NOTOOLTIP", clicked = function() menu.current = menu.leveltype end}),
        button:new({text = "Options", x = 5, y = 10, tooltip = "Audio and graphics settings", clicked = function() menu.current = menu.options end}),
        button:new({text = "Quit", x = 5, y = 12, tooltip = "Bye :(", clicked = function() print("Closing game...") os.exit() end}),
    },

    options = {
        slider:new({text = "Framerate", x = 12.5, y = 4, tooltip = "Controls FPS (0 = unlimited).", value = 60, max = 180, clicked = function(self) settings.fpslimit = self.value end}),
        slider:new({text = "Quality", x = 12.5, y = 8, tooltip = "Does nothing... yet.", value = 3, max = 3, clicked = function(self) settings.quality = self.value end}),
        slider:new({text = "Volume", x = 12.5, y = 12, value = 100, max = 100, clicked = function(self) for i, v in ipairs(data.sound) do v:setVolume(self.value/1000) end end,}),
        button:new({text = "Back to Main", x = 12.5, y = 16, tooltip = "Return to Main menu.", clicked = function() menu.current = menu.main end}),
    },

    leveltype = {
        button:new({text = "Arcade mode", x = 12.5, y = 2, tooltip = "Currently unstable game mode based on old arcade shooters.", clicked = function() INGAME = true end}),
        button:new({text = "Back to Main", x = 12.5, y = 14, tooltip = "Return to Main menu.", clicked = function() menu.current = menu.main end}),
    },

    arcadeselect = {
        
    }
})

menu.current = menu.main

function menu:update(dt)
    for i, v in ipairs(self.current) do
        v:update(dt)
    end
end

function menu:draw()
    if self.current == self.main then
        love.graphics.setFont(data.fonts.zom100)
        love.graphics.setColor(255,0,0)
        love.graphics.print("Mr Shotgun",WIDTH/3.6, 50)

        love.graphics.setFont(data.fonts.pix24)
        love.graphics.setColor(179, 179, 179)

        love.graphics.print("Copyright (c) 2015 Josh O'Leary, GNU GPL 2.0", WIDTH-340, HEIGHT-20)
    end

    for i, v in ipairs(self.current) do
        v:draw()
    end
end