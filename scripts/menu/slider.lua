slider = button:new({value = 0, currentAmount = 1000, min = 0, max = 100, h = 3, lockedin = false})

-- droow it
function slider:draw()
	love.graphics.setColor(179, 179, 179)

    love.graphics.rectangle("fill", self.x*32, self.y*32, self.w*32, self.h*32)

    love.graphics.setColor(0,0,0)
    love.graphics.setFont(data.fonts.pix40)
    love.graphics.print(self.text..": "..self.value, self.x*32+4, self.y*32+5)

    love.graphics.setColor(80,80,80)
    love.graphics.rectangle("fill", self.x*32+16, self.y*32+48, self.w*32-32, 32)

    if self.mouseover or self.lockedin then
        love.graphics.setColor(204,120,41)
        love.graphics.rectangle("fill", self.x*32+16-4+self.currentAmount, self.y*32+48-4, 20+8, 32+8)
    else
        love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill", self.x*32+16-2+self.currentAmount, self.y*32+48-2, 20+4, 32+4)
    end

    if self.mouseover == true then
        if self.tooltip == "NOTOOLTIP" then

        else
            love.graphics.setColor(80,80,80)
            love.graphics.rectangle("fill", love.mouse.getX()+2, love.mouse.getY()-30, self.tooltip:len()*8, 20)

            love.graphics.setFont(data.fonts.pix24)
            love.graphics.setColor(255,255,255)
            love.graphics.print(self.tooltip, love.mouse.getX()+4, love.mouse.getY()-27)
        end
    end
end

function slider:update(dt)

    if aabb(love.mouse.getX(), love.mouse.getY(), 1, 1, self.x*32+16-2+self.currentAmount, self.y*32+48-2, 20+4, 32+4) then
        self.mouseover = true
    else
        self.mouseover = false
    end

    if love.mouse.isDown("l") and self.mouseover == true then
        self.canClick = false

        self.lockedin = true
    end

    if love.mouse.isDown("l") == false then
        self.canClick = true
        self.lockedin = false
    end

    local balls = self.x*32+16
    local balls2 = self.x*32+16 + self.w*32-32
    local balls3 = balls2 - balls - 20

    if self.lockedin then
        self.currentAmount = love.mouse.getX() - balls
        self:clicked(self)
    end

    if self.currentAmount < 0 then self.currentAmount = 0 end
    if self.currentAmount > balls3 then self.currentAmount = balls3 end

    self.value = round(self.currentAmount*(self.max) / balls3)
end