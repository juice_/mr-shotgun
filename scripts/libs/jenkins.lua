-- We called it jenkins cause we don't know what to name our function storage


-- for debug messages
function debug(msg)
	if DEBUG then print(msg) end
end

-- collisions
function aabb(ax, ay, aw, ah, bx, by, bw, bh)
	local ax2,ay2,bx2,by2 = ax + aw, ay + ah, bx + bw, by + bh
	return ax < bx2 and ax2 > bx and ay < by2 and ay2 > by
end

-- rounding numbers
function round(num, idp)
	local mult = 10^(idp or 0)
	return math.floor(num * mult + 0.5) / mult
end

function dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

table.indexOf = function(t, object)
    if "table" == type(t) then
        for i = 1, #t do
            if object == t[i] then
                return i
            end
        end
        return -1
    else
        error("table.indexOf expects table for first argument, " .. type(t) .. " given")
    end
end

-- object orientation
metaClass = {}

function metaClass:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end