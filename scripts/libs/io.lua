require "json"

io = {}

function io.delete(item)
    
    -- recursively remove any items inside folder then remove folder/file
    if love.filesystem.isDirectory(item) then
        for _, child in pairs(love.filesystem.getDirectoryItems(item)) do
            recursivelyDelete(item .. '/' .. child, depth + 1)
            love.filesystem.remove(item .. '/' .. child)
        end
    elseif love.filesystem.isFile(item) then
        love.filesystem.remove(item)
    end
    love.filesystem.remove(item)
end

function io.log(string)
    love.filesystem.write("log_"+json.encode(string)
    