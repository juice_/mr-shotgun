-- render the stuff
function gameDraw()

	print(10 / 5)

	-- render ingayme
	if INGAME then

        -- background
	    love.graphics.setColor(0,0,0)
	    love.graphics.rectangle("fill",0,0,WIDTH,HEIGHT)

	    -- rnd
	    renderMap(currentMap, 1)

		-- draw entity shit
		love.graphics.setColor(255,255,255,255)
		player:draw()

		for i, v in ipairs(currentMap.entities) do
			v:draw()
		end
		

		renderMap(currentMap, 2)

		-- draw debug shit
		if DEBUG then
			love.graphics.setColor(255,255,255)
			love.graphics.setFont(data.fonts.defaultFont)
			love.graphics.print("FPS: "..tostring(love.timer.getFPS()), 10, 0)
			love.graphics.print("x: "..round(player.x/64, 0)..", y: "..round(player.y/64, 0), 10, 60)
			love.graphics.print("velocity: "..round(player.velocity, 2),10, 40)
			love.graphics.print("Mem (KB): "..round(collectgarbage("count"), 1), 10, 20)
		end


		-- UI
		love.graphics.setColor(255,0,0, 128)
		love.graphics.rectangle("fill", WIDTH-200,0,200,20)

		love.graphics.setColor(0,255,0, 128)
		love.graphics.rectangle("fill",WIDTH-200,0, (200) / (player.maxHealth) * (player.health), 20)


		if STEP == false then
			love.graphics.setColor(0,0,0,128)
			love.graphics.rectangle("fill", 0, 0, WIDTH, HEIGHT)

		end
	elseif INGAME == false then
		-- otherwise just draw menu
		menu:draw()
	end

	-- fps limit and counter
	local curTime = love.timer.getTime()
	if nextTime <= curTime then
		nextTime  = curTime
		return
	end
	love.timer.sleep(nextTime - curTime)
end
