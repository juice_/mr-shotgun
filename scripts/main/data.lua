-- Data for all of the game. Images, audio, other shit too --

data = {
	textures = {
		grass = love.graphics.newImage("textures/grass.png"),
		player = love.graphics.newImage("textures/playerBody.png"),
		tree = love.graphics.newImage("textures/tree.png"),
		wall = love.graphics.newImage("textures/wall.png"),
		corner = love.graphics.newImage("textures/corner.png")
	},

	sound = {
		shotgun = love.audio.newSource("audio/shotgun.wav", "static")
	},

	fonts = {
		defaultFont = love.graphics.newFont(12),

		pix12 = love.graphics.newFont("fonts/pixel.ttf", 12),
		pix24 = love.graphics.newFont("fonts/pixel.ttf", 24),
		pix40 = love.graphics.newFont("fonts/pixel.ttf", 40),

		zom100 = love.graphics.newFont("fonts/zombified.ttf", 100),
		zom70 = love.graphics.newFont("fonts/zombified.ttf", 70)
	},

	cursors = {
		reddotsight = love.mouse.newCursor("textures/cursor.png", 16, 16)
	}
}