-- logic block for the whole game
local deb

function gameUpdate(dt)

	-- fps counter
	nextTime = nextTime + (1 / settings.fpslimit)

	-- mouse

	mouseX = love.mouse.getX()
	mouseY = love.mouse.getY()

	-- ingame update of stuff
	if INGAME then
		if love.keyboard.isDown("escape") and deb == true then
			STEP = not STEP
			deb = false
		end

		if not love.keyboard.isDown("escape") then
			deb = true
		end
		-- update player

		-- STEP is a variable that allows pausing
		if STEP then
			player:update(dt)
		
			for i, v in ipairs(currentMap.entities) do
				v:update(dt)
			end
		else

		end
	else
		menu:update(dt)
	end
end
