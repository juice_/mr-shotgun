------------------------------------------------------------------------------------------------
-- Top down zombie shooty game: "Mr Shotgun" by Joshua O'Leary (Juice, Csharp0, PinkFloyd666) --
------------------------------------------------------------------------------------------------

--[[
	Copyright (c) 2015 Joshua O'Leary

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	
	Running of this game means acceptance of the above terms.
]]--

-- Initialize the game
function love.load()

	-- debug info for startup
	print("")
	print("Loading libraries...")

	-- load textures, music etc.
	require "scripts/main/data"

	-- import draw and update methods
	require "scripts/main/update"
	require "scripts/main/draw"

	-- import important libraries
	json = require "scripts/libs/json"
	require "scripts/libs/jenkins"

	-- require game data
	require "scripts/menu/menu"
	require "scripts/entity/entity"
	require "scripts/player/player"
	require "scripts/bullet/bullet"
	require "scripts/enemy/enemy"
	require "scripts/entity/particle"
	require "scripts/map/map"


	print("Data loaded successfully")

	-- global variables mostly for debugging
	DEBUG = true
	INGAME = false
	VERSION = "0.1.22"
	WIDTH = 1024
	HEIGHT = 608
	STEP = true

	-- settings
	settings = {
		quality = 2,
		soundEnabled = true,
		volume = 6,
		fpslimit = 60
	}

	-- fps thingy
	nextTime = love.timer.getTime()

	-- mouse variables
	mouseX = 0
	mouseY = 0

	-- camera
	cameraX = 0
	cameraY = 0

	-- start the rendering
	print("Mr Shotgun v"..VERSION.." by Joshua O'Leary")
	debug("Attempting to start graphics window...")
	love.window.setMode(WIDTH, HEIGHT, {resizable = false, vsync = false})
	love.window.setTitle("Mr Shotgun v"..VERSION)

	-- cursor
	love.mouse.setCursor(data.cursors.reddotsight)
end


-- Keyboard stoof lol
function love.keyreleased(k)
	if INGAME and k == "f1" then DEBUG = not DEBUG end

	if INGAME and k == "f2" then end
end

-- logic block for the whole game
function love.update(dt) gameUpdate(dt) end


-- render the stuff
function love.draw() gameDraw() end
